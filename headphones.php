<!DOCTYPE html>
<html ng-app="prj" ng-controller="ctrl">
<head>
	<meta charset="utf-8"/>
	<title>DEV1CESH0P</title>
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/angular.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<link href="template.less" rel="stylesheet/less"/>
	<script src="js/less.min.js"></script>
  	<script type="text/javascript" src="js/scp.js"></script>
  	<script type="text/javascript" src="js/main.js"></script>
  	<script type="text/javascript" src="js/template.js"></script>
</head>
<body ng-app="prj" ng-controller="ctrl" ng-init="init()">
<header>
  <nav class="navbar navbar-inverse bar" style="border-bottom:1px solid #E6E6E6;">
  <div class="container-fluid">
    <div class="navbar-header barheader" >
      <a class="navbar-brand barheader" href="index.php">DEV1CESH0P</a>
    </div>
    <ul id="lol" class="nav navbar-nav">
      <li class=""><a href="index.php">HOME</a></li>
      <li><a href="delivery.php">Оплата/Доставка</a></li>
      <li><a href="contacts.php">Контакты</a></li>
      <li><form class="navbar-form" role="search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search..." name="srch-term" id="srch-term" style="border-radius: 7px;width: 100px;z-index: 0;"
                ng-style="focused && {'width':'200px','transition':'400ms linear'} || {'width':'100px','transition':'400ms linear'}" ng-init="focused = false" ng-focus="focused = true;showfilter=true" ng-blur="focused = false" ng-change="finditem(inpsearch,findsect)" ng-model="inpsearch" />
            </div>
        </form>
      </li>
      <li>
      	   <select class="form-control" style="margin-top: 8px;margin-left: -15px;min-width: 130px;" ng-show="showfilter" ng-model="findsect" ng-options="i for i in sections">
			    <option value="">All</option>
		   </select>
	</li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<?php
      session_start();
      if(!empty($_SESSION['username'])){
          echo '<li><a href="#">'.$_SESSION['username'].'</a></li>
          		<li><a href="#" ng-click="logout()">Log out</a></li>
          		<li><a href="basket.php" ng-init="initbasketnum()">{{basketnum}} items in basket</a></li>';
      }
      else{
      	echo '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Вход<span class="caret"></span></a>
        <ul class="dropdown-menu">
	          <li><a href="#" id="signup">Регистрация</a></li>
          	  <li><a href="#" id="signin">Авторизация</a></li>
        </ul>
      </li>';
      }

      ?>
      <li><a class="cart" href="basket.php"></a></li>
      
      </ul>
  </div>
</nav>
</header>
</div>

<div id="createpost">
  <div class="overlaypost"></div>
  <div class="contentpost">
  	  <p>Category: </p><select class="form-control" ng-model="slctd">
        <option>Headphones</option>
        <option>Mouse</option>
        <option>Keyboard</option>
      </select>
      <label for="titlepost">Title: </label><br><input class="form-control" type="text" id="titlepost" ng-model="titlepost" maxlength="30" style="width:280px;display: inline-block;" /><p style="display: inline-block;">&nbsp;{{30-titlepost.length}}</p><br>
      <label for="pricepost">Price: $ </label><input class="form-control" type="text" id="pricepost" ng-model="pricepost"/><br>
      <label for="image">Image: </label><input id="image" class="form-control" type="text" ng-model="img" /><br>
	<button class="btn btn-primary" style="display: block;margin: auto;" ng-click="crt(slctd,titlepost,pricepost,img)">Create Post</button>
  </div>

</div>


<div id="loginpop">
  <div class="overlay"></div>
  <div class="content">
      <label for="userlog">Username: </label><input class="form-control" type="text" id="userlog" ng-model="userlog"/><br>
      <label for="passlog">Password: </label><input class="form-control" type="password" id="passlog" ng-model="passlog"/><br>
      <button class="btn btn-primary" style="display: block;margin: auto;" ng-click="sign(userlog,passlog)">Sign in</button>
  </div>
</div>

<div id="regpop">
  <div class="overlayreg"></div>
  <div class="contentreg">
  	  <label for="nameinp">Name: </label><br><input class="form-control" type="text" id="nameinp" ng-model="nameinp" maxlength="140" style="display: inline-block;width: 450px;" /><p style="display: inline-block;">&nbsp;{{40-nameinp.length}}</p><br>
      <label for="usernameinp">Username: </label><br><input class="form-control" type="text" id="usernameinp" ng-model="usernameinp" maxlength="50" style="display: inline-block;width: 450px;"/><p style="display: inline-block;">&nbsp;{{50-usernameinp.length}}</p><br>
      <label for="passwordinp">Password: </label><br><input class="form-control" type="password" id="passwordinp" ng-model="passwordinp" maxlength="100" style="display: inline-block;width: 450px;"/><p style="display: inline-block;">&nbsp;{{100-passwordinp.length}}</p><br>
      <button type="button" class="btn btn-primary" style="display: block;margin: auto;" ng-click="reg(nameinp,usernameinp,passwordinp)">Sign Up</button>
  </div>
</div>
<div class="searchdiv" ng-style="showfind" ng-show="focused">
	<div ng-repeat="i in finditems" >
		<a href="#"><img ng-src={{i.image}} width="120px;"/></a>
		<p class="titl">{{i.title}}</p>
		<p class="prc">{{i.price}}$</p>
	</div>
</div>
<div class="container" style="z-index: -1">
		<div class="col-md-2" style="margin-top:-20px">
			<div id='categorymenu' style="margin-left: -100px;">
				<ul>
				   <li class="first"><a href='#'><span>Home</span></a></li>

				   <li><a href='#'><span class="drp">Products</span></a>
				      <ul>
				         <li><a href='headphones.php'>Headphones</a></li>
				         <li><a href='keyboards.php'>Keyboards</a></li>
				         <li><a href='mouses.php'>Mouses</a></li>
				      </ul>
				   </li>

				   <li><a href='#'><span class="drp">Company</span></a>
				      <ul>
				         <li><a href='about.php'>About</a></li>
				         <li><a href='delivery.php'>Delivery</a></li>
				      </ul>
				   </li>

				   <li><a href='contacts.php'><span>Contact</span></a></li>
				</ul>
			</div>
		</div>
		<div class="col-md-8" style="margin-top:-20px;">
			<div id="slider">
			<a href="#/" class="control left" hidden="">Left</a>
			<a href="#/" class="control right" hidden="">Right</a>
				<div class="frame">
					<ul class="items" id="items">
						<li><img src='images/1.jpg'/></li>
						<li><img src='images/2.jpg'/></li>
						<li><img src='images/3.jpg'/></li>
						<li><img src='images/4.jpg'/></li>
						<li><img src='images/5.jpg'/></li>
						<li><img src='images/6.jpg'/></li>
					</ul>
				</div>
			</div>    
			<?php
		      if(!empty($_SESSION['username'])){
		      	if($_SESSION['type']==1){
		      		echo '<button id="crtpost" class="btn btn-primary" style="display:block;margin:auto">Create post</button><br>';
		      	}
		      }
		       ?>
		  <div class="row">
		    <nav class="navbar navbar-default" style="width:730px;margin-left:15px;">
				<div class="container-fluid">
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="#">{{items}} items in store</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#">Sort by:</a></li>
							<li>
								<select class="form-control" ng-model="sort" ng-change="sortbyprice(sort)" style="margin-top: 10px;">
							        <option>Price: high to low</option>
							        <option>Price: low to high</option>
							    </select>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		    <div class="col-md-4" ng-repeat="x in posts | limitTo:limit" style="margin-bottom: 10px;"> 
		      <div class="panel panel-default">
		        <div class="panel-heading"><a href="tovar/{{x.id}}.php" class="posttitle">{{x.title}}</a></div>
		        <div class="panel-body"><a href="tovar/{{x.id}}.php"><img ng-src={{x.image}} class="img-responsive" style="width:250px;height: 185px" alt="Image"/></a></div>
		        <div class="panel-footer" style="text-align: center">{{x.price}}$</div>
		        <div id="editpost">
			  		<div class="overlayedit"></div>
			  		<div class="contentedit">
			  	  		<p>Category: </p><select class="form-control" ng-model="editselected">
			        		<option>Headphone</option>
			        		<option>Mouse</option>
			        		<option>Keyboard</option>
			      		</select>
			      		<label for="titleedit">Title: </label><input class="form-control" type="text" id="titleedit" ng-model="titleedit"/><br>
			      		<label for="priceedit">Price: $ </label><input class="form-control" type="text" id="priceedit" ng-model="priceedit"/><br>
			      		<label for="image">Image: </label><input id="image" class="form-control" type="text" ng-model="imgedit" /><br>
						<button class="btn btn-primary" style="display: block;margin: auto;" ng-click="editpostgo(editselected,titleedit,priceedit,imgedit)">Edit Post</button>
			  		</div>
				</div>
		      </div>
		      <?php
		      if(!empty($_SESSION['username'])){
		      	if($_SESSION['type']==1){
		      		echo '<button class="btn btn-primary" style="margin-left:15px;margin-top:-10px;" ng-click="editpost(x.id)">Edit</button><button class="btn btn-danger" style="margin-left:75px;margin-top:-10px;" ng-click="deletepost(x)">Delete</button>';
		      	}
		      	else if($_SESSION['type']==0){
		      		echo '<button class="btn btn-default" style="margin-left:70px;margin-top:-10px;" ng-click="addtoBasket(x.id)">Add to cart</button>';
		      	}
		      }
		       ?>
		    </div>
		    <button ng-click="loadmore()" class="btn btn-default" style="display: block;width: 100%;">Load more</button>
		  </div>
		</div>
		<div class="col-md-2" style="margin-top:-20px;">
			
		</div>
</div>
<footer class="container-fluid text-center">
  <p>Online Store Copyright</p>  
</footer>
</body>
<script>
		function loginpopupHide() {
            $('#loginpop .content').animate({'margin-top':-1000}, 500, function() {
                $('#loginpop .overlay').fadeOut(300, function() {
                    $('#loginpop').hide();
                });
            });
        }
  		function regpopupHide() {
            $('#regpop .contentreg').animate({'margin-top':-1000}, 500, function() {
                $('#regpop .overlayreg').fadeOut(300, function() {
                    $('#regpop').hide();
                });
            });
        }
        function createhide() {
            $('#createpost .contentpost').animate({'margin-top':-1000}, 500, function() {
                $('#createpost .overlaypost').fadeOut(300, function() {
                    $('#createpost').hide();
                });
            });
        }
        function editpop() {
            $('#editpost').show();
            $('#editpost .overlayedit').fadeIn(300, function() {
                $('#editpost .contentedit').animate({'margin-top':200}, 500);
            });
        }

        function edithide() {
            $('#editpost .contentedit').animate({'margin-top':-1000}, 500, function() {
                $('#editpost .overlayedit').fadeOut(300, function() {
                    $('#editpost').hide();
                });
            });
        }

  		var app = angular.module("prj",[]);
		app.controller("ctrl",function($scope,$http) {
		$scope.posts = [];

		$scope.init = function(){
			$http.get("http://localhost/final/requests/showposts.php").then(function(response){
				//$scope.posts = angular.fromJson(response.data);
				var res = angular.fromJson(response.data);
				for(var i = 0;i<res.length;i++){
					if(res[i].category=="Headphones"){
						$scope.posts.push(res[i]);
					}
				}
				$scope.items = response.data.length;
			});
		}
	
		$scope.sign = function(userlog,passlog){
			var url = "http://localhost/final/requests/signin.php";
			  $http.get(url+ "?username="+userlog+"&password="+passlog).then(function(response){
			  	 window.location.href = 'index.php';
			  	 var d = angular.fromJson(response.data);
			  	 alert("Hello "+d[0]+" you're succesfully signed!");
			  	 loginpopupHide();
			  });
		}
		$scope.reg = function(nameinp,usernameinp,passwordinp){
			  var url = "http://localhost/final/requests/reg.php";
			  $http.get(url+ "?name="+nameinp+"&username="+usernameinp+"&password="+passwordinp).then(function(response){
			  	if(response.data=="no"){
			  		alert("this user is already registred!");
			  	}
			  	else{
			  	 alert(response.data+" succesfully registred! Sign in please!");
			  	 regpopupHide();
			  	}
			  });
	    }
	    $scope.logout = function(){
	    	var url = "http://localhost/final/requests/logout.php";
	    	 $http.get(url+"?logout=logout").then(function(response){
	    	 		alert("You're "+response.data+"!");
	    	 		window.location.href = 'index.php';
	    	 });
	    }
	    $scope.crt = function(category,title,price,img){
	    	var url = "http://localhost/final/requests/createpost.php";
	    	$http.get(url+"?category="+category+"&title="+title+"&price="+price+"&image="+img).then(function(response){
	    		$scope.titlepost = "";
	    		$scope.img = "";
	    		$scope.pricepost = "";
	    		createhide();
	    		$scope.init();
	    	});
	    }
	    $scope.idthis = "";
	    $scope.editpost = function(x){
	    	$scope.idthis = x;
	    	var url = "http://localhost/final/requests/info.php";
	    	$http.get(url+"?id="+x).then(function(response){
	    	    var arr = angular.fromJson(response.data);
	    	    $scope.editselected = arr[0].category;
	    	    $scope.titleedit = arr[0].title;
	    	    $scope.priceedit = arr[0].price;
	    	    $scope.imgedit = arr[0].image;
	    	});
	    	editpop();
	    }
	    $scope.editpostgo = function(category,title,price,image){
	    	var url = "http://localhost/final/requests/editpost.php";
$http.get(url+"?id="+$scope.idthis+"&titleEdit="+title+"&category="+category+"&price="+price+"&image="+image).then(function(response){
				console.log(response.data);
				$scope.init();
	    	});
	    	edithide();
	    }
	    $scope.deletepost = function(x){
	    	var url = "http://localhost/final/requests/deletepost.php";
	    	$http.get(url+"?id="+x.id).then(function(response){
	    		$scope.init();
	    	});
	    }
	    $scope.sortbyprice = function(tip){
	    	if(tip=="Price: low to high"){
	    			$http.get("http://localhost/final/requests/byprice.php").then(function(response){
	    			$scope.items = response.data.length;
					$scope.posts = angular.fromJson(response.data);
				});
	    	}
	    	else{
	    			$http.get("http://localhost/final/requests/bypricehigh.php").then(function(response){
	    			$scope.items = response.data.length;
					$scope.posts = angular.fromJson(response.data);
				});
	    	}	
	    }
	    $scope.finditems = [];
	    $scope.finditem = function(inp,findsect){
	    	if(inp!=""){
	    	var url = "http://localhost/final/requests/finder.php";
	    	$http.get(url+"?input="+inp+"&sect="+findsect).then(function(response){
	    		var res = angular.fromJson(response.data);
	    		if(res=="not found"){
	    			$scope.finditems = [];
	    			$scope.showfind = {'display':'none'};
	    		}
	    		else{
	    			$scope.showfind = {'display':'inline-block'};
	    			$scope.finditems = res;
	    		}
	    	});
	       }
	       if(inp=="" || inp==undefined || inp == null){
	           $scope.showfind = {'display':'none'};
	           $scope.finditems = [];
	       }
	    }
	    $scope.sections = ['Headphones','Keyboard','Mouse'];
	    $scope.limit = 3;
	    $scope.loadmore = function(){
	    	var increamented = $scope.limit + 3;
	      	if(increamented>$scope.posts.length){
	        	$scope.limit = $scope.posts.length;
	      	}
	      	else{
	        	$scope.limit = increamented;
	      	}
	    }
	    $scope.initbasketnum = function(){
	    	var url = "http://localhost/final/requests/shownumbasket.php";
	    	$http.get(url).then(function(response){
	    		$scope.basketnum = response.data;
	    	});
	    }

	    $scope.addtoBasket = function(id){
	    	var url = "http://localhost/final/requests/tobasket.php";
	    	$http.get(url+"?id="+id).then(function(response){
	    		alert(response.data);
	    		$scope.initbasketnum();
	    	});
	    }
	});

  	</script>
</html>
