$(document).ready(function() {
        function loginpopup() {
            $('#loginpop').show();
            $('#loginpop .overlay').fadeIn(300, function() {
                $('#loginpop .content').animate({'margin-top':200}, 500);
            });

        }

        function loginpopupHide() {
            $('#loginpop .content').animate({'margin-top':-1000}, 500, function() {
                $('#loginpop .overlay').fadeOut(300, function() {
                    $('#loginpop').hide();
                });
            });
        }

        $('#signin').click(function() {
            loginpopup();
        });

        $('#loginpop .overlay').click(function() {
            loginpopupHide();
        });

        function regpopup() {
            $('#regpop').show();
            $('#regpop .overlayreg').fadeIn(300, function() {
                $('#regpop .contentreg').animate({'margin-top':200}, 500);
            });

        }

        function regpopupHide() {
            $('#regpop .contentreg').animate({'margin-top':-1000}, 500, function() {
                $('#regpop .overlayreg').fadeOut(300, function() {
                    $('#regpop').hide();
                });
            });
        }

        $('#signup').click(function() {
            regpopup();
        });

        $('#regpop .overlayreg').click(function() {
            regpopupHide();
        });

        function createpop() {
            $('#createpost').show();
            $('#createpost .overlaypost').fadeIn(300, function() {
                $('#createpost .contentpost').animate({'margin-top':200}, 500);
            });

        }

        function createhide() {
            $('#createpost .contentpost').animate({'margin-top':-1000}, 500, function() {
                $('#createpost .overlaypost').fadeOut(300, function() {
                    $('#createpost').hide();
                });
            });
        }

        $('#crtpost').click(function() {
            createpop();
        });

        $('#createpost .overlaypost').click(function() {
            createhide();
        });

        function editpop() {
            $('#editpost').show();
            $('#editpost .overlayedit').fadeIn(300, function() {
                $('#editpost .contentedit').animate({'margin-top':200}, 500);
            });
        }

        function edithide() {
            $('#editpost .contentedit').animate({'margin-top':-1000}, 500, function() {
                $('#editpost .overlayedit').fadeOut(300, function() {
                    $('#editpost').hide();
                });
            });
        }

        $('#editpostbtn').click(function() {
            editpop();
        });

        $('#editpost .overlayedit').click(function() {
            edithide();
        });

    $('.nav li').click(function() {
        var slideToggle = this;
        if ($('ul', this).is(':visible')) {
            $('ul', this).slideUp(function() {
                $(slideToggle).removeClass('active');
            });
        }
        else {
            $('.dropdown-menu').slideUp();
            $('ul', this).slideDown();
            $(slideToggle).addClass('active');
        }
    });
});