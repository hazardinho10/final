$(document).ready(function(){

  $('#categorymenu > ul > li:has(ul)').addClass("has-sub");

  $('#categorymenu > ul > li > a').click(function() {
    var checkElement = $(this).next();  
    if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
      checkElement.slideUp('normal');
    }
    if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
      $('#categorymenu ul ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
    }		
  });

});